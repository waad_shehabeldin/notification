<?php

namespace intrazero\notification;

class notification
{
    public function notification(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}